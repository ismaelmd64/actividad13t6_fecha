package tema6.actividad13;

public class TestFecha {
    /**
     * A Continuación se presentan algunos ejemplos de uso de la clase Fecha, que deberas borrar
     * Para realizar el ejercicio.
     *
     * @param args
     */
    public static void main(String[] args) {
        //Comprobar Constructores
        System.out.println("=== START - Prueba de Constructores - START ===\n");
        // Fecha1
        System.out.println("--- Creo un nuevo objeto utilizando el constructor parametrizado int int int (16,1,2021) ---");
        Fecha primeraFecha=new Fecha(16,1,2021);
        primeraFecha.mostrarFormatoES();
        primeraFecha.mostrarFormatoGB();
        primeraFecha.mostrarFormatoTexto();
        System.out.println("La fecha es correcta: "+primeraFecha.isCorrecta());
        System.out.println("La fecha es festivo: "+primeraFecha.isFestivo());
        System.out.println("El dia de la semana es: "+primeraFecha.getDiaSemana());
        System.out.println("-------------------------------\n");

        //Fecha2
        System.out.println("--- Creo un nuevo objeto utilizando la fecha anterior mediante el constructor por copia ---");
        Fecha segundaFecha=primeraFecha.clone();
        segundaFecha.mostrarFormatoES();
        segundaFecha.mostrarFormatoGB();
        segundaFecha.mostrarFormatoTexto();
        System.out.println("La fecha es correcta: "+segundaFecha.isCorrecta());
        System.out.println("La fecha es festivo: "+segundaFecha.isFestivo());
        System.out.println("El dia de la semana es: "+segundaFecha.getDiaSemana());
        System.out.println("--------------------------------------\n");

        //Comprobacion metodo isEqual
        System.out.println("--- Compruebo mediante el método equals que las fechas representadas por ambos objetos son iguales ---");
        System.out.println("La fecha creada con los constructores anteriores son iguales: "+primeraFecha.isEqual(segundaFecha));
        System.out.println("-------------------------------\n");

        //Fecha3
        System.out.println("--- Creo un nuevo objeto utilizando el constructor String 16/1/2021 ---");
        Fecha terceraFecha=new Fecha("16/1/2021");
        terceraFecha.mostrarFormatoES();
        terceraFecha.mostrarFormatoGB();
        terceraFecha.mostrarFormatoTexto();
        System.out.println("La fecha es correcta: "+terceraFecha.isCorrecta());
        System.out.println("La fecha es festivo: "+terceraFecha.isFestivo());
        System.out.println("El dia de la semana es: "+terceraFecha.getDiaSemana());
        System.out.println("-------------------------------\n");

        //Fecha4
        System.out.println("--- Creo un nuevo objeto utilizando el constructor por defecto ---");
        Fecha cuartaFecha=new Fecha();
        cuartaFecha.mostrarFormatoES();
        cuartaFecha.mostrarFormatoGB();
        cuartaFecha.mostrarFormatoTexto();
        System.out.println("La fecha es correcta: "+cuartaFecha.isCorrecta());
        System.out.println("La fecha es festivo: "+cuartaFecha.isFestivo());
        System.out.println("El dia de la semana es: "+cuartaFecha.getDiaSemana());
        System.out.println("-------------------------------\n");

        //Fecha4.2
        System.out.println("--- Creo un nuevo objeto utilizando el constructor el constructor parametrizado Fecha (primeraFecha)");
        Fecha cuartaFecha2=new Fecha(primeraFecha);
        cuartaFecha2.mostrarFormatoES();
        cuartaFecha2.mostrarFormatoGB();
        cuartaFecha2.mostrarFormatoTexto();
        System.out.println("La fecha es correcta: "+cuartaFecha2.isCorrecta());
        System.out.println("La fecha es festivo: "+cuartaFecha2.isFestivo());
        System.out.println("El dia de la semana es: "+cuartaFecha2.getDiaSemana());
        System.out.println("-------------------------------\n");
        System.out.println("=== FIN - Prueba de Constructores - FIN ===\n");

        //Comprobar Metodos
        System.out.println("=== START - Prueba de Métodos anyadir/restar días - START ====\n");
        //Comprobacion1
        System.out.println("--- Día siguiente a la fecha inicial (16-1-2021) - (+1 día) ---");
        Fecha quintaFecha=primeraFecha.anyadir(1);
        quintaFecha.mostrarFormatoES();
        quintaFecha.mostrarFormatoGB();
        quintaFecha.mostrarFormatoTexto();
        System.out.println("La fecha es correcta: "+quintaFecha.isCorrecta());
        System.out.println("La fecha es festivo: "+quintaFecha.isFestivo());
        System.out.println("El dia de la semana es: "+quintaFecha.getDiaSemana());
        System.out.println("-------------------------------\n");
        //Comprobacion2
        System.out.println("--- Día anterior a la fecha inicial (16-1-2021) - (-1 día) ---");
        Fecha sextaFecha=primeraFecha.restar(1);
        sextaFecha.mostrarFormatoES();
        sextaFecha.mostrarFormatoGB();
        sextaFecha.mostrarFormatoTexto();
        System.out.println("La fecha es correcta: "+sextaFecha.isCorrecta());
        System.out.println("La fecha es festivo: "+sextaFecha.isFestivo());
        System.out.println("El dia de la semana es: "+sextaFecha.getDiaSemana());
        System.out.println("-------------------------------\n");
        //Comprobacion3
        System.out.println("--- Fecha correspondiente a restar 30 días a la fecha inicial (16-1-2021) - (-30 días) ---");
        Fecha septimaFecha=primeraFecha.restar(30);
        septimaFecha.mostrarFormatoES();
        septimaFecha.mostrarFormatoGB();
        septimaFecha.mostrarFormatoTexto();
        System.out.println("La fecha es correcta: "+septimaFecha.isCorrecta());
        System.out.println("La fecha es festivo: "+septimaFecha.isFestivo());
        System.out.println("El dia de la semana es: "+septimaFecha.getDiaSemana());
        System.out.println("-------------------------------\n");
        System.out.println("=== FIN - Prueba de Métodos anyadir/restar - FIN ===\n");

        //Modificar fecha con metodo set(int,int,int)
        System.out.println("=== START - Prueba del método modificador - START ===\n");
        //Modificado1
        System.out.println("--- Modifico la fecha del primer objeto creado (16-1-2020) por la fecha 22-1-2021 ---");
        primeraFecha.set(22,1,2021);
        primeraFecha.mostrarFormatoES();
        primeraFecha.mostrarFormatoGB();
        primeraFecha.mostrarFormatoTexto();
        System.out.println("La fecha es correcta: "+primeraFecha.isCorrecta());
        System.out.println("La fecha es festivo: "+primeraFecha.isFestivo());
        System.out.println("El dia de la semana es: "+primeraFecha.getDiaSemana());
        System.out.println("-------------------------------\n");
        System.out.print("=== FIN - Prueba del método modificador - FIN ===");


    }

}
