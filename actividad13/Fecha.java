package tema6.actividad13;
import java.util.StringTokenizer;

public class Fecha {
    private static final int MAX_MESES=12;
    private static final int MAX_DIAS_SEMANA=7;
    private static final int MAX_DIAS_MESES=31;
    private static final int DIAS_ANYO=365;

    private int dia;

    private int mes;

    private int anyo;

    private static final String[] DIAS_TEXTO = new String[] { "domingo", "lunes", "martes", "miercoles", "jueves", "viernes",
            "sábado"};

    private static final String[] MESES_TEXTO = new String[] { "enero", "febrero", "marzo", "abril", "mayo", "junio",
            "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };

    /**
     *  Constructor por defecto
     *  Inicializa una fecha a dia 1-1-1970
     */
    public Fecha() {
        this.dia=1;
        this.mes=1;
        this.anyo=1970;
    }

    /**
     *  Inicializa la fecha
     *  @param dia de la semana
     *  @param mes del año
     *  @param anyo
     */
    public Fecha(int dia, int mes, int anyo) {
        this.dia=dia;
        this.mes=mes;
        this.anyo=anyo;
    }

    /**
     * Crea una fecha a partir de otra fecha pasada como argumento
     *
     * Deberemos hacer uso de los métodos consultores para inicializar nuestra clase
     *
     * @param fecha
     */
    public Fecha(Fecha fecha) {
        dia=fecha.getDia();
        mes=fecha.getMes();
        anyo=fecha.getAnyo();
    }

    /**
     * Inicializa la fecha a partir de otra pasada en formato String dd/mm/yyyy
     *
     * Deberemos trocearlas de forma que asignemos el día més y año a cada uno de los atributoe
     * @param fecha
     */
    public Fecha(String fecha) {
        StringTokenizer datos=new StringTokenizer(fecha);
        dia=Integer.parseInt(datos.nextToken("/"));
        mes=Integer.parseInt(datos.nextToken("/"));
        anyo=Integer.parseInt(datos.nextToken("/"));
    }

    /**
     * Modifica la fecha actual a partir de los datos pasados como argumento
     */
    public void set(int dia, int mes, int anyo) {
        this.dia=dia;
        this.mes=mes;
        this.anyo=anyo;
    }

    /**
     * Devuelve el día de la semana que representa por la Fecha actual
     * @return @dia
     */
    public int getDia() {
        return dia;
    }

    /**
     * Devuelve el mes que representa la Fecha actual
     * @return @mes
     */
    public int getMes(){
        return mes;
    }

    /**
     * Devuelve el año que representa la Fecha actual
     * @return @mes
     */
    public int getAnyo(){
        return anyo;
    }

    public Fecha clone(){
        return new Fecha(dia, mes, anyo);
    }

    /**
     * Muestra por pantalla la fecha en formato español dd-mm-yyyy
     */
    public void mostrarFormatoES()  {
        System.out.printf("%02d-%02d-%04d\n",dia,mes,anyo);
    }

    /**
     * Muestra por pantalla la fecha en formato inglés yyyy-mm-dd
     */
    public void mostrarFormatoGB() {
        System.out.printf("%04d-%02d-%02d\n",anyo,mes,dia);
    }

    /**
     * Muestra por pantalla la fecha en formato texto dd-mmmm-yyyy
     * Ej. 1 enero de 1970
     */
    public void mostrarFormatoTexto() {
        System.out.printf("%02d-"+getMesTexto()+"-%04d\n",dia,anyo);
    }

    /**
     * Retorna un booleano indicando si la fecha del objeto es igual a la fecha pasada como
     * argumento
     *
     * @return boolean
     */
    public boolean isEqual(Fecha otraFecha) {
        if (otraFecha.dia==dia && otraFecha.mes==mes && otraFecha.anyo==anyo ){
            return true;
        }else {
        return false;}
    }

    /**
     * Retorna el dia correspondiente de la semana en formato String
     * @return String
     */
    public String getDiaSemana() {
        int diasTranscurridos=getDiasTranscurridosOrigen();
        int resto=diasTranscurridos%MAX_DIAS_SEMANA;
        return DIAS_TEXTO[resto];
    }

    /**
     * Solo Festivo sábado o domingo
     * @return boolean
     */
    public boolean isFestivo() {
        if (getDiaSemana().equals(DIAS_TEXTO[6]) || getDiaSemana().equals(DIAS_TEXTO[0])){
            return true;
        }else {
        return false;}
    }

    /**
     * Devuelve un objeto de tipo fecha que representa una fecha añadiendo  @numDias
     * A la fecha Actual. El número máximo de dias a restar es 30
     *
     * @return boolean
     */
    public Fecha anyadir(int numDias){
        if (numDias<0 || numDias>30){
            return new Fecha(dia,mes,anyo);
        }else {
            int diaNuevo = dia;
            int mesNuevo = mes;
            int anyoNuevo = anyo;
            for (int i = 0; i < numDias; i++) {
                diaNuevo++;
                if (diaNuevo == getDiasMes(mesNuevo, anyoNuevo)) {
                    diaNuevo = 1;
                    mesNuevo++;
                    if (mesNuevo > MAX_MESES) {
                        mesNuevo = 1;
                        anyoNuevo++;
                    }
                }
            }
            return new Fecha(diaNuevo, mesNuevo, anyoNuevo);
        }
    }

    /**
     * Devuelve un objeto de tipo fecha que representa una fecha restando @numDias
     * A la fecha Actual. El número máximo de dias a restar es 30
     *
     * @return boolean
     */

    public Fecha restar(int numDias){
        if (numDias<0 || numDias>30){
            return new Fecha(dia,mes,anyo);
        }else {
            int diaNuevo = this.dia;
            int mesNuevo = this.mes;
            int anyoNuevo = this.anyo;
            for (int i = 0; i < numDias; i++) {
                diaNuevo--;
                if (diaNuevo == 0) {
                    if ((mesNuevo - 1) == 0) {
                        anyoNuevo--;
                        diaNuevo = MAX_DIAS_MESES;
                        mesNuevo = MAX_MESES;

                    } else {
                        diaNuevo = getDiasMes(mesNuevo - 1, anyo - 1);
                        mesNuevo--;
                    }
                }
            }
            return new Fecha(diaNuevo, mesNuevo, anyoNuevo);
        }
    }

    public boolean isCorrecta(){
        if (isDiaCorrecto() && isMesCorrecto() && anyo>0){
            return true;
        }
        return false;
    }

    private boolean isDiaCorrecto(){
        if (dia<=getDiasMes(mes,anyo) && dia>0){
            return true;
        }
        return false;
    }

    private boolean isMesCorrecto(){
        if (mes<=MAX_MESES && mes>0){
            return true;
        }
        return false;
    }

    /**
     * Retorna el dia correspondiente de la semana en formato caracter
     * @return char
     */
    private String getMesTexto() {
        return MESES_TEXTO[(mes-1)];
    }

    /**
     * Devuelve el número de dias transcurridos desde el 1-1-1
     *
     * @return int
     */
    private int getDiasTranscurridosOrigen() {
        int diasTranscurridos=0;
        for (int i=1;i<anyo;i++){
            diasTranscurridos+=getDiasAnyo(i);
        }
        diasTranscurridos+=getDiasTranscurridosAnyo();
        return diasTranscurridos;
    }

    /**
     * Devuelve el número de dias transcurridos en el anyo actual
     *
     * @return int
     */
    private int getDiasTranscurridosAnyo() {
        int diasTrancurridos=0;
        for (int i=1;i<mes;i++){
            diasTrancurridos+=getDiasMes(i,anyo);
        }
        diasTrancurridos+=dia;
        return diasTrancurridos;
    }

    /**
     * Indica si el año pasado como argumento es bisiesto
     * Un año es bisiesto si es divisible por 4 a su vez 100 por 400
     *
     * @return boolean
     */

    public static boolean isBisiesto(int anyo){
        if (anyo % 4 == 0) {
            if (anyo % 100 != 0) {
                return true;
            }
            return (anyo % 400 == 0);
        }
        return false;
    }

    /**
     *  Calcula el número de días que tiene el mes representado por la fecha actual
     *
     *  @return int total dias mes en curso
     */
    public static int getDiasMes(int mes, int anyo) {
        if( mes == 4 || mes == 6 || mes == 9
                || mes == 11) {
            return 30;
        }else if (mes == 2){
            if (isBisiesto(anyo)){
                return  29;
            }else return   28;
        }
        return MAX_DIAS_MESES;
    }

    /**
     * Calcula el número total de dias que tiene el año pasado como argumento
     *
     * @return int total dias anyo en curso
     */
    public static int getDiasAnyo(int anyo){
        if(isBisiesto(anyo)){
            return 366;
        }else
        return DIAS_ANYO;
    }

}
